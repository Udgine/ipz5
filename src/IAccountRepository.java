
package Repository;

import model.Account;

import java.util.List;

public interface IAccountRepository
{

    Account findByName ( String name );

    List<Account> getQuery ();

}
