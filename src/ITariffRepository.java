package Repository;

import model.Tariff;

import java.util.List;

/**
 * Created by Евгений on 20.10.2016.
 */
public interface ITariffRepository {

    Tariff findByName (String name);

    List<Tariff> getQuery ();

}
