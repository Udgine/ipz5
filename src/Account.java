package model;

import utils.DomainEntity;

import java.io.Serializable;

public class Account extends DomainEntity implements Serializable{
    private String name;
    private String email;
    private String password;
    private String imageURL;
    private Contact contact;
    private Tariff tariff;
    private InternetInfo internetInfo;
    private TariffState tariffState;
    private ConnectedServices connectedServices;
    private String number;
    private Balance balance;

    public InternetInfo getInternetInfo() {
        return internetInfo;
    }

    public void setInternetInfo(InternetInfo internetInfo) {
        this.internetInfo = internetInfo;
    }

    public ConnectedServices getConnectedServices() {
        return connectedServices;
    }

    public void setConnectedServices(ConnectedServices connectedServices) {
        this.connectedServices = connectedServices;
    }

    public TariffState getTariffState() {
        return tariffState;
    }

    public void setTariffState(TariffState tariffState) {
        this.tariffState = tariffState;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Balance getBalance() {
        return balance;
    }

    public void setBalance(Balance balance) {
        this.balance = balance;
    }

    public Tariff getTariff() {
        return tariff;
    }

    public void setTariff(Tariff tariff) {
        this.tariff = tariff;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public Contact getContact() {
        return contact;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }

    public String getName(){
        return name;
    }

    public void setName(String name){
        this.name = name;
    }

    public String getEmail(){
        return email;
    }

    public void setEmail(String email){
        this.email = email;
    }

    public String getPassword(){
        return password;
    }

    public void setPassword(String password){
        this.password = password;
    }

    public Account(){

    }

    public Account(Integer uuid, String name, String email, String password, Contact contact, String imageURL,
                   Tariff tariff, InternetInfo internetInfo, TariffState tariffState, ConnectedServices connectedServices, String number, Balance balance){
        super(uuid);
        this.name = name;
        this.email = email;
        this.password = password;
        this.contact = contact;
        this.imageURL = imageURL;
        this.tariff = tariff;
        this.internetInfo = internetInfo;
        this.tariffState = tariffState;
        this.connectedServices = connectedServices;
        this.number = number;
        this.balance = balance;
    }

    @Override
    public String toString(){
        return String.format("===%s===\n", "Account "+ getDomainId() + " " + name + " " + email + " " + password + " " + contact + " " + imageURL + " "
                + tariff + " " + internetInfo + " " + tariffState + " " + connectedServices + " " + number + " " + balance);
    }

    public boolean checkPassword(String password){
         if (password == null)
             throw new IllegalArgumentException("password");

        return this.password == password;
    }
}
