package DAO;

import com.mysql.jdbc.Statement;
import model.Tariff;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class TariffDAO {

    private static final String SQL_UPDATE_TARIFF = "UPDATE tariffs SET id_account, name = ? WHERE id = ? ";
    private static final String SQL_INSERT_INTO_TARIFF = "INSERT INTO tariffs (id, id_account, name) VALUES (?,?,?)";
    private static final String SQL_SELECT_BY_NAME = "SELECT * FROM tariffs WHERE name = ?";

    public void create(Connection connection, Tariff tariff) throws SQLException {
        PreparedStatement prStatement = connection.prepareStatement(SQL_INSERT_INTO_TARIFF,
                Statement.RETURN_GENERATED_KEYS);
        setInsertStatementParameters(tariff, prStatement);
        prStatement.executeUpdate();
        prStatement.close();
    }

    public void update(Connection connection, Tariff tariff) throws SQLException {
        PreparedStatement prStatement = connection.prepareStatement(SQL_UPDATE_TARIFF);
        setUpdateStatementParameters(tariff, prStatement);
        prStatement.executeUpdate();
        prStatement.close();
    }

    public List<Tariff> getByName(Connection connection, String name) throws SQLException {
        PreparedStatement prStatement = connection.prepareStatement(SQL_SELECT_BY_NAME);
        prStatement.setString(1, name);
        ResultSet rs = prStatement.executeQuery();
        List<Tariff> tariffs = getTariffs(rs);
        prStatement.close();
        return tariffs;
    }

    public List<Tariff> getTariffs(ResultSet rs) throws SQLException {
        List<Tariff> tariffs = new ArrayList<Tariff>();
        while (rs.next())
            tariffs.add(getTariff(rs));
        return tariffs;
    }

    public Object result (ResultSet rs, String str){
        Object object = null;
        try {
            byte[] array = (byte[]) rs.getObject(str);
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(array);
            ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
            object = objectInputStream.readObject();
        }
        catch (SQLException | IOException | ClassNotFoundException e){
            e.printStackTrace();
        }
        return object;
    }

    private Tariff getTariff(ResultSet rs) throws SQLException {
        Tariff tariff = new Tariff();
        tariff.setId(rs.getInt("id"));
        tariff.setIdAccount(rs.getInt("id_account"));
        tariff.setName(rs.getString("name"));
        return tariff;
    }

    private void setUpdateStatementParameters(Tariff tariff, PreparedStatement prStatement) throws SQLException {
        int k = 1;
        prStatement.setInt(k++, tariff.getIdAccount());
        prStatement.setString(k++, tariff.getName());
        prStatement.setInt(k++, tariff.getId());
    }

    private void setInsertStatementParameters(Tariff tariff, PreparedStatement prStatement) throws SQLException {
        int k = 1;
        prStatement.setInt(k++, tariff.getId());
        prStatement.setInt(k++, tariff.getIdAccount());
        prStatement.setString(k++, tariff.getName());
    }
}
