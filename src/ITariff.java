package service;

import DTO.TariffDTO;

import java.util.List;

/**
 * Created by Евгений on 19.10.2016.
 */
public interface ITariff {

    List<TariffDTO> viewAllTariffs();
	
	public Integer create ( String name );

    TariffDTO findTariffByName(String name);

}
