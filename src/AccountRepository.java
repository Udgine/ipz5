package Repository.jpa;

import Repository.IAccountRepository;
import model.Account;
import model.forDAO.AccountService;

import java.util.List;

/**
 * Created by Евгений on 20.10.2016.
 */
public class AccountRepository implements IAccountRepository
{

    public List<Account> query;
    private AccountService service;


    public AccountRepository ( AccountService entityManager )
    {
        service = entityManager;
        query = entityManager.getAccounts();
    }



    @Override
    public Account findByName ( String name )
    {
        List<Account>  res =service.getAccountByName(name);
        return ( res.size() == 1 ) ? res.get( 0 ) : null;
    }

    @Override
    public List<Account> getQuery() {
        return query;
    }


}