package service.impl;

import DTO.ServiceDTO;
import Repository.jpa.ServiceRepository;
import model.ConnectedServices;
import model.Service;
import service.IService;

import java.util.List;

/**
 * Created by Евгений on 21.10.2016.
 */
public class ServService implements IService {

    private ServiceRepository serviceRepository;
	
	private Random random;

    public ServService(ServiceRepository serviceRepository, Random random )
    {
        this.serviceRepository = serviceRepository;
		this.random = random;
    }


    @Override
    public void ConnectService (Integer idAcc, String nameServ, Integer idServ)
    {
        List<ConnectedServices> data = serviceRepository.getQuery();
        for (int i=0; i < data.size(); i++)
        {

            if (data.get(i).getIdAccount() == idAcc)
            {
                Service serv = new Service(idServ, nameServ);
                data.get(i).connectService(serv);

                }
        }

    }

    @Override
    public void CanselService (Integer idAcc, String nameServ)
    {
        List<ConnectedServices> data = serviceRepository.getQuery();
        for (int i=0; i < data.size(); i++)
        {

            if (data.get(i).getIdAccount() == idAcc)
            {
                    List<Service> data2 = data.get(i).getServiceList();
                for (int j = 0; j < data2.size(); j++) {
                    if (data2.get(j).getName() == nameServ)
                        data2.remove(j);
                    data.get(i).setServiceList(data2);
                }
            }
        }

    }

	@Override
    public Integer create ( String name )
    {
        if ( ServiceRepository.findByName( name ) != null )
            throw new IllegalStateException( "Duplicate service " + name );

        Service service = new Service( , name );

        ServiceRepository.getQuery.add(service);

        return service.getUUID();
    }
	
    public ServiceDTO findServiceByName(String name)
    {
        List<ConnectedServices> data = serviceRepository.getQuery();

        for (int i=0; i < data.size(); i++)
        {
            List<Service> data2 = data.get(i).getServiceList();
            for (int j=0; j< data2.size();j++)
            {
                if (data2.get(j).getName() == name)
                    return DtoBuilder.toDto(data2.get(j));
            }
        }

        return null;
    }

}
