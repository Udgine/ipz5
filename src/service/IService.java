package service;

import DTO.ServiceDTO;

/**
 * Created by Евгений on 19.10.2016.
 */
public interface IService {

	Integer create(String name);

    void ConnectService (Integer idAcc, String nameServ, Integer idServ);

    void CanselService (Integer idAcc, String nameServ);

    ServiceDTO findServiceByName(String name);

}
