package service;

import DTO.BalanceDTO;

import java.util.List;

/**
 * Created by Евгений on 19.10.2016.
 */
public interface IBalance {

    List<BalanceDTO> ViewUnconfirmedBalanceChanges(Integer accountId );

	Integer create(double value);
	
    List<BalanceDTO> ViewBalanceChangeDetails(Integer accountId);


}
