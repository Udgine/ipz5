package service;


import DTO.*;

import java.util.List;

public interface IAccount {

    AccountDto identify (String name, String password );

    BalanceDTO showBalance(double value);
	
	Integer create(String name, String email, String password, Contact contact, String imageURL,
                   Tariff tariff, InternetInfo internetInfo, TariffState tariffState, ConnectedServices connectedServices, String number, Balance balance);

    List<ServiceDTO> ShowConnectedServices(String name);

    TariffDTO ShowTariffDetails(String name);

    void addBalance ( String name, double balance );

    void uploadImage ( String name, String url );

    ContactDTO viewContactDetails(String adress);

    void editContactDetails ( String name, String address, String phone );

    TariffDTO viewTariffDetails(String name);

    List<TariffDTO> viewUnconfirmedTariffs();

    ServiceDTO viewServiceDetails(String name);

    List<TariffDTO> viewUnconfirmedTariffs(String name);

    void changeTariff(String name, String tariffName);


}
