package DTO;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Евгений on 18.10.2016.
 */
public class ServiceDTO extends DomainEntityDto<ServiceDTO>{

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ServiceDTO(Integer domainId, String name) {
        super(domainId);
        this.name = name;
    }

    public List< Object > getAttributesToIncludeInEqualityCheck ()
    {
        return Arrays.asList( getName());
    }
}
