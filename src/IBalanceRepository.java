package Repository;

import model.Balance;

import java.util.List;

/**
 * Created by Евгений on 20.10.2016.
 */
public interface IBalanceRepository {

    Balance findByValue( double value);

    List<Balance> getQuery ();



}
