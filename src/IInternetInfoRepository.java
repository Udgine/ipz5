package Repository;

import model.InternetInfo;

import java.util.List;

/**
 * Created by Евгений on 20.10.2016.
 */
public interface IInternetInfoRepository {

    InternetInfo findByName (String name);

    List<InternetInfo> getQuery ();

}
