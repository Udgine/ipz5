package model.forDAO;

import DAO.ConnectedServicesDAO;
import DAO.ConnectionFactory;
import model.ConnectedServices;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

public class ConnectedServicesService {

    private ConnectedServicesDAO connectedServicesDAO;

    public ConnectedServicesService(ConnectedServicesDAO connectedServicesDAO) {
        this.connectedServicesDAO = connectedServicesDAO;
    }

    public void createConnectedServices(ConnectedServices connectedServices) throws SQLException {
        Connection connection = ConnectionFactory.getConnection();
        try {
            connectedServicesDAO.create(connection, connectedServices);
            connection.commit();
        } catch (SQLException e) {
            connection.rollback();
            throw e;
        }
    }

    public void updateConnectedServices(ConnectedServices connectedServices) throws SQLException {
        Connection connection = ConnectionFactory.getConnection();
        try {
            connectedServicesDAO.update(connection, connectedServices);
            connection.commit();
        } catch (SQLException e) {
            connection.rollback();
            throw e;
        }
    }

    public List<ConnectedServices> getConnectedServicesByServiceList(String serviceList) throws SQLException {
        Connection connection = ConnectionFactory.getConnection();
        try {
            List<ConnectedServices> connectedServices = connectedServicesDAO.getByServiceList(connection, serviceList);
            connection.commit();
            return connectedServices;
        } catch (SQLException e) {
            connection.rollback();
            throw e;
        }
    }

    public List<ConnectedServices> getConntectedServices() throws SQLException {
        Connection connection = ConnectionFactory.getConnection();
        Statement stmt = connection.createStatement();
        ResultSet rs = stmt.executeQuery("SELECT * FROM connectedservicess");

        try {
            List<ConnectedServices> accounts = connectedServicesDAO.getConnectedServices(rs);
            connection.commit();
            return accounts;
        } catch (SQLException e) {
            connection.rollback();
            throw e;
        }
    }



}
