package model;

import java.util.UUID;

public abstract class Entity {
    private Integer uuid;

    public Integer getId(){
        return uuid;
    }

    public void setId(Integer uuid){
        this.uuid = uuid;
    }

    protected Entity(){

    }

    protected Entity(Integer uuid){
        if (uuid == null)
            throw new IllegalArgumentException("id");

        this.uuid = uuid;
    }

    @Override
    public boolean equals(Object object){
        if (this == object)
            return true;

        if (object == null || this.getClass() != object.getClass())
            return false;

        Entity entity = (Entity)object;
        return uuid == entity.uuid;
    }

    @Override
    public int hashCode() {
        return uuid.hashCode();
    }
}
