package model.forDAO;

import DAO.ConnectionFactory;
import DAO.InternetInfoDAO;
import model.InternetInfo;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

public class InternetInfoService {

    private InternetInfoDAO internetInfoDAO;

    public InternetInfoService(InternetInfoDAO internetInfoDAO) {
        this.internetInfoDAO = internetInfoDAO;
    }

    public void createnternetInfo(InternetInfo internetInfo) throws SQLException {
        Connection connection = ConnectionFactory.getConnection();
        try {
            internetInfoDAO.create(connection, internetInfo);
            connection.commit();
        } catch (SQLException e) {
            connection.rollback();
            throw e;
        }
    }

    public void updatenternetInfo(InternetInfo internetInfo) throws SQLException {
        Connection connection = ConnectionFactory.getConnection();
        try {
            internetInfoDAO.update(connection, internetInfo);
            connection.commit();
        } catch (SQLException e) {
            connection.rollback();
            throw e;
        }
    }

    public List<InternetInfo> getInternetInfoByName(String name) throws SQLException {
        Connection connection = ConnectionFactory.getConnection();
        try {
            List<InternetInfo> internetInfos = internetInfoDAO.getByName(connection, name);
            connection.commit();
            return internetInfos;
        } catch (SQLException e) {
            connection.rollback();
            throw e;
        }
    }

    public List<InternetInfo> getInternetInfos() throws SQLException {
        Connection connection = ConnectionFactory.getConnection();
        Statement stmt = connection.createStatement();
        ResultSet rs = stmt.executeQuery("SELECT * FROM internetinfos");

        try {
            List<InternetInfo> accounts = internetInfoDAO.getInternetInfos(rs);
            connection.commit();
            return accounts;
        } catch (SQLException e) {
            connection.rollback();
            throw e;
        }
    }


}
