package model.forDAO;

import DAO.ConnectionFactory;
import DAO.TariffStateDAO;
import model.TariffState;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

public class TariffStateService {

    private TariffStateDAO tariffStateDAO;

    public TariffStateService(TariffStateDAO tariffStateDAO) {
        this.tariffStateDAO = tariffStateDAO;
    }

    public void createTariffState(TariffState tariffState) throws SQLException {
        Connection connection = ConnectionFactory.getConnection();
        try {
            tariffStateDAO.create(connection, tariffState);
            connection.commit();
        } catch (SQLException e) {
            connection.rollback();
            throw e;
        }
    }

    public void updateTariffState(TariffState tariffState) throws SQLException {
        Connection connection = ConnectionFactory.getConnection();
        try {
            tariffStateDAO.update(connection, tariffState);
            connection.commit();
        } catch (SQLException e) {
            connection.rollback();
            throw e;
        }
    }

    public List<TariffState> getTariffStateByName(String name) throws SQLException {
        Connection connection = ConnectionFactory.getConnection();
        try {
            List<TariffState> tariffStates = tariffStateDAO.getByName(connection, name);
            connection.commit();
            return tariffStates;
        } catch (SQLException e) {
            connection.rollback();
            throw e;
        }
    }

    public List<TariffState> getTariffStates() throws SQLException {
        Connection connection = ConnectionFactory.getConnection();
        Statement stmt = connection.createStatement();
        ResultSet rs = stmt.executeQuery("SELECT * FROM connectedservicess");

        try {
            List<TariffState> accounts = tariffStateDAO.getTariffStates(rs);
            connection.commit();
            return accounts;
        } catch (SQLException e) {
            connection.rollback();
            throw e;
        }
    }


}
