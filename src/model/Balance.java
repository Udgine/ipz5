package model;

import utils.DomainEntity;

import java.io.Serializable;

public class Balance extends DomainEntity implements Serializable {
    private double balanceValue;
    private Integer idAccount;

    public Integer getIdAccount() {
        return idAccount;
    }

    public void setIdAccount(Integer idAccount) {
        this.idAccount = idAccount;
    }

    public static final int MAXBALANCEVALUE = 2000;
    public static final int MINBALANCEVALUE = 0;

    public double getBalanceValue() {
        return balanceValue;
    }

    public Balance(){

    }

    public void setBalanceValue(double balanceValue) {
        this.balanceValue = balanceValue;
    }

    public Balance(Integer uuid, double balanceValue) {
        super(uuid);
        if (balanceValue < MINBALANCEVALUE || balanceValue > MAXBALANCEVALUE)
            throw new IllegalArgumentException("balance");

        this.balanceValue = balanceValue;
    }

    @Override
    public String toString(){
        return String.format("===%s===\n", balanceValue);
    }
}
