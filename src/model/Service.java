package model;

import utils.DomainEntity;

import java.io.Serializable;

public class Service extends DomainEntity implements Serializable {
    private String name;

    public Service(Integer uuid, String name){
        super(uuid);
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString(){
        return String.format("===%s===\n", getDomainId() + " " + name);
    }
}
