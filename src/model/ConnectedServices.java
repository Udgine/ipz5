package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ConnectedServices  extends Entity implements Serializable {
    private List<Service> serviceList;
    private Integer idAccount;

    public Integer getIdAccount() {
        return idAccount;
    }

    public void setIdAccount(Integer idAccount) {
        this.idAccount = idAccount;
    }

    public ConnectedServices(){
        serviceList = new ArrayList<Service>();
    }

    public List<Service> getServiceList() {
        return serviceList;
    }

    public void setServiceList(List<Service> serviceList) {
        this.serviceList = serviceList;
    }

    public void connectService(Service service){

        if (service == null)
            throw new IllegalArgumentException("service");

        serviceList.add(service);
    }

    @Override
    public String toString(){
        return String.format("===%s===\n", serviceList);
    }
}
