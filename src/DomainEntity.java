

package utils;

public abstract class DomainEntity
{

    protected DomainEntity ()
    {
    }

    protected DomainEntity ( Integer domainId )
    {

        if ( domainId == null )
            throw new IllegalArgumentException( "domainId" );

        this.domainId = domainId;
    }

    public Integer getDomainId ()
    {
        return this.domainId;
    }

    public void setDomainId (Integer id) { this.domainId = id;}

    public long getDatabaseId ()
    {
        return this.databaseId;
    }

    public void setDatabaseId ( long databaseId )
    {
        this.databaseId = databaseId;
    }

    @Override
    public boolean equals ( Object o )
    {
        if ( o.getClass() != this.getClass() )
            return false;

        DomainEntity otherDomainEntity = ( DomainEntity ) o;
        return domainId.equals( otherDomainEntity.getDomainId() );
    }


    private Integer domainId;

    private long databaseId;
}
