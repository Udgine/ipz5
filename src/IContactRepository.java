package Repository;

import model.Contact;

import java.util.List;


public interface IContactRepository  {

    Contact findByAdress (String adress );
    List<Contact> getQuery ();


}
