package DTO;

import java.util.Arrays;
import java.util.List;

public class AccountDto extends DomainEntityDto<AccountDto>{

    private String name;
    private String email;
    private String password;
    private String imageURL;
    private ContactDTO contact;
    private TariffDTO tariff;
    private InternetInfoDTO internetInfo;
    private TariffStateDTO tariffState;

    private String number;
    private BalanceDTO balance;

    public String getName() {return name;}
    public String getEmail() {
        return email;
    }
    public String getPassword() { return password; }
    public String getImageURL() { return imageURL; }
    public ContactDTO getContact() { return  contact;}
    public TariffDTO getTariff() {return tariff;}
    public InternetInfoDTO getInternetInfo() {return internetInfo;}
    public TariffStateDTO getTariffState() {return tariffState;}
    public String getNumber() { return  number;}
    public BalanceDTO getBalance() { return balance;}


    public void setName(String name) { this.name = name; }
    public void setEmail(String email) {
        this.email = email;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }
    public void setContact(ContactDTO contact) {this.contact = contact; }
    public void setTariff(TariffDTO tariff) {this.tariff = tariff; }
    public void setInternetInfo(InternetInfoDTO internetInfo) { this.internetInfo = internetInfo;}
    public void setTariffState(TariffStateDTO tariffState) { this.tariffState = tariffState;}
    public void setNumber(String number) { this.number = number;}
    public void setBalance(BalanceDTO balance) { this.balance = balance;}

    public AccountDto(Integer uuid){
        super(uuid);
    }

    public AccountDto ( Integer domainId, String name,
                        String email,
                        String password,
                        String imageURL,
                        ContactDTO contact,
                        TariffDTO tariff,
                        InternetInfoDTO internetInfo,
                        TariffStateDTO tariffState,
                        String number,
                        BalanceDTO balance)
    {
        super( domainId );
        this.name = name;
        this.email = email;
        this.password = password;
        this.imageURL = imageURL;
        this.contact = contact;
        this.tariff = tariff;
        this.internetInfo = internetInfo;
        this.tariffState = tariffState;
        this.number = number;
        this.balance = balance;

    }

    @Override
    public String toString(){
        return String.format(
                "name = %s\nemail = %s\npassword = %s\nimageURL = %s\ncontact = %s\ntariff = %s\nInternet = %s\nTariffState = %s" +
                                               "\nnumber = %s\nbalance = %s\n",
                getName(),
                getEmail(),
                getPassword(),
                getImageURL(),
                getContact().toString(),
                getTariff().toString(),
                getInternetInfo().toString(),
                getTariffState().toString(),
                getNumber(),
                getBalance().toString()
        );
    }

    public List< Object > getAttributesToIncludeInEqualityCheck ()
    {
        return Arrays.asList( getName(),
                getEmail(),
                getPassword(),
                getImageURL(),
                getContact(),
                getTariff(),
                getInternetInfo(),
                getTariffState(),
                getNumber(),
                getBalance().toString()
        );
    }


}
