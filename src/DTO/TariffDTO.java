package DTO;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Евгений on 18.10.2016.
 */
public class TariffDTO extends DomainEntityDto<TariffDTO>{

    private String name;
    private Integer idAccount;

    public Integer getIdAccount() {
        return idAccount;
    }

    public void setIdAccount(Integer idAccount) {
        this.idAccount = idAccount;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public TariffDTO(Integer domainId, String name, Integer idAccount) {
        super(domainId);
        this.name = name;
        this.idAccount = idAccount;
    }

    public List< Object > getAttributesToIncludeInEqualityCheck ()
    {
        return Arrays.asList(getName(), getIdAccount());
    }
}
